# ToDo application 
The application uses clean architecture.

## Development process

PhpSpec is used to design the classes. Development starts with the domain. 

For a simple use case such as Add a todo

    Todo can be added with a description

The domain is created first. This is under the domain directory. This layer has no knowledge about the client application that will use this or
the framework that is going to wire the details. This layer is the most abstract layer, describing the domain and what you can do with it using use cases.

Then one moves on to creating the infrastructure components and use a framework, symfony in this case to wire them all together and provide a web interface.

## Structure
    domain - most abstract and innermost layer
    database - Anything to do with the database. For example the todo repository that deals with the persistence to a database. These are 'driven' code that are invoked from the web or a mobile ui.
    web - symfony application that provides the web ui. This layer references wires together other layers using dependency injection 
    
# Web - symfony application

Doctrine is configured to look up from the domain code. The mappings also reside in this layer. 

Repositories are pointed to the appropriate implementations from the database layer in services.yaml

PhpSpec is used to design controllers, also symfony controller tests and/or behat tests can be added here.

# Why not keep everything in one project created by symfony

We want the abstract domain code to be independent if we were to follow clean architecture. Placing this in the symfony created code base will mean that it is tied to the symfony structure at least semantically.

Components that utilise the framework's features should be tightly coupled to the framework and be placed here, and that does n't necessarily use the frameworks features are separated to achieve the perfect balance of cohesion and coupling.

In this case the domain and database are reusable, if you want to use laravel instead of symfony for example.

This is a highly scalable model of code organisation. Different developers can work on different components independently without worrying about breaking the application because they code to interfaces.


