<?php

namespace spec\Todo\Database;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Todo\Database\TodoRepository;
use Todo\Domain\Todo;
use Todo\Domain\TodoRepositoryInterface;

/**
 * Class TodoRepositorySpec
 * @package spec\Todo\Database
 * @mixin TodoRepository
 */
class TodoRepositorySpec extends ObjectBehavior
{
    function let(EntityManagerInterface $entityManager)
    {
        $this->beConstructedWith($entityManager);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(TodoRepository::class);
        $this->shouldImplement(TodoRepositoryInterface::class);
    }

    function it_saves_todo(Todo $todo, EntityManagerInterface $entityManager)
    {
        $entityManager->persist($todo)->shouldBeCalled();
        $entityManager->flush()->shouldBeCalled();
        $this->save($todo);
    }
}
