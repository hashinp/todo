<?php

namespace spec\App\Controller;

use App\Controller\AddTodo;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AddTodoSpec extends ObjectBehavior
{
    function let(\Todo\UseCases\AddTodo $addTodoUseCase)
    {
        $this->beConstructedWith($addTodoUseCase);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(AddTodo::class);
    }

    function it_adds_todos(\Todo\UseCases\AddTodo $addTodoUseCase, Request $request)
    {
        $description = 'buy milk';
        $request->get('description')->willReturn($description);
        $addTodoUseCase->add($description)->shouldBeCalled();
        $this->__invoke($request)->shouldHaveType(Response::class);
    }
}
