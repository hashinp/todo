<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Todo\UseCases\AddTodo as AddTodoUseCase;

class AddTodo extends AbstractController
{
    /**
     * @var AddTodoUseCase
     */
    private $addTodo;

    public function __construct(AddTodoUseCase $addTodo)
    {
        $this->addTodo = $addTodo;
    }

    /**
     * @Route("/add/{description}", name="todo_add")
     */
    public function __invoke(Request $request)
    {
        $description = $request->get('description');
        $this->addTodo->add($description);
        return new Response('todo added');
    }
}
