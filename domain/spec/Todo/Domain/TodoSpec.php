<?php

namespace spec\Todo\Domain;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Todo\Domain\Todo;

/**
 * Class TodoSpec
 * @package spec\Todo\Domain
 * @mixin Todo
 */
class TodoSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('buy milk');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Todo::class);
    }

    function it_has_as_description()
    {
        $this->getDescription()->shouldBe('buy milk');
    }
}
