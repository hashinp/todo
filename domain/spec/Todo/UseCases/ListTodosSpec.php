<?php

namespace spec\Todo\UseCases;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Todo\Domain\Todo;
use Todo\Domain\TodoRepositoryInterface;
use Todo\UseCases\ListTodos;
use Todo\UseCases\ListTodosOutputInterface;

/**
 * Class ListTodosSpec
 * @package spec\Todo\UseCases
 * @mixin ListTodos
 */
class ListTodosSpec extends ObjectBehavior
{
    function let(TodoRepositoryInterface $todoRepository, ListTodosOutputInterface $output)
    {
        $this->beConstructedWith($todoRepository, $output);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ListTodos::class);
    }

    function it_lists_todos(
        TodoRepositoryInterface $todoRepository,
        Todo $buyMilk,
        Todo $fileTaxes,
        ListTodosOutputInterface $output
    ) {
        $todos = [$buyMilk, $fileTaxes];
        $todoRepository->findAll()->willReturn($todos);
        $output->list($todos)->shouldBeCalled();
        $this->listTodos();
    }
}
