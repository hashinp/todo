<?php

namespace spec\Todo\UseCases;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Todo\Domain\Todo;
use Todo\Domain\TodoRepositoryInterface;
use Todo\UseCases\AddTodo;

/**
 * Class AddTodoSpec
 * @package spec\Todo\UseCases
 * @mixin AddTodo
 */
class AddTodoSpec extends ObjectBehavior
{
    function let(TodoRepositoryInterface $todoRepository)
    {
        $this->beConstructedWith($todoRepository);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(AddTodo::class);
    }

    function it_can_add_a_todo(TodoRepositoryInterface $todoRepository)
    {
        $todoRepository->save(Argument::that(function(Todo $todo) {
            return $todo->getDescription() == 'buy milk';
        }))->shouldBeCalled();

        $this->add('buy milk');
    }
}
