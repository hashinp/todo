<?php

namespace Todo\UseCases;

use Todo\Domain\Todo;
use Todo\Domain\TodoRepositoryInterface;

class AddTodo
{
    /**
     * @var TodoRepositoryInterface
     */
    private $todoRepository;

    public function __construct(TodoRepositoryInterface $todoRepository)
    {
        $this->todoRepository = $todoRepository;
    }

    public function add(string $description): void
    {
        $todo = new Todo($description);
        $this->todoRepository->save($todo);
    }
}
