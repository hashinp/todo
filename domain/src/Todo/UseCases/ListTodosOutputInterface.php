<?php

namespace Todo\UseCases;

interface ListTodosOutputInterface
{
    public function list(array $todos): void;
}
