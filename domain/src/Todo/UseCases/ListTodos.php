<?php

namespace Todo\UseCases;

use Todo\Domain\TodoRepositoryInterface;

class ListTodos
{
    /**
     * @var TodoRepositoryInterface
     */
    private $todoRepository;
    /**
     * @var ListTodosOutputInterface
     */
    private $listTodosOutput;

    public function __construct(TodoRepositoryInterface $todoRepository, ListTodosOutputInterface $listTodosOutput)
    {
        $this->todoRepository = $todoRepository;
        $this->listTodosOutput = $listTodosOutput;
    }

    public function listTodos()
    {
        $todos = $this->todoRepository->findAll();
        $this->listTodosOutput->list($todos);
    }
}
