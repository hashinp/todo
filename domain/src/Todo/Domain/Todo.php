<?php

namespace Todo\Domain;

class Todo
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $description;

    public function __construct(string $description)
    {
        $this->description = $description;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}
