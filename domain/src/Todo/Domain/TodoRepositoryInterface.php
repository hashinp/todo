<?php

namespace Todo\Domain;

interface TodoRepositoryInterface
{
    public function save(Todo $todo): void;

    public function findAll(): array;
}
